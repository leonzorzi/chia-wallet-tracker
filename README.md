# Chia-Wallet-Tracker

## About

A Python script to check your [Chia Network](https://www.chia.net/) (Farming-)Crypto-Wallet balance which sends you a Telegram notification whenever it increases.


## Google Cloud Platform

This Project is deployed as a GCP Serverless Function and utilizes a Key-Value Datastore to compare and update the wallet balance.


## API Dependencies

[XCH Scan](https://xchscan.com/rest-api) <br />
[Telegram Bot](https://core.telegram.org/bots/api) <br />
[GCP Datastore](https://cloud.google.com/datastore/docs/apis)

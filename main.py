import os
import requests
import json
import dotenv
import sys


from dotenv import load_dotenv
from requests.exceptions import HTTPError
from google.cloud import datastore


def update_balance(newBalanceValue):
    """Updates the balance json file in Google DataStore"""

    try:
        # Instantiates client
        client = datastore.Client()

        # The kind for the new entity
        kind = "wallet-balance-data"
        # The name/ID for the new entity
        name = balanceDataEntity

        with client.transaction():
            key = client.key(kind, name)
            task = client.get(key)

            task["lastBalanceValue"] = newBalanceValue

            client.put(task)

    except Exception as err:
        print(err)
        send_notification("update_balance%20Error-please%20check%20logs")
        sys.exit(1)


def get_last_balance():
    """Downloads balance json file data from Google DataStore"""

    try:
        # Instantiates client
        datastore_client = datastore.Client()

        # The kind for the new entity
        kind = "wallet-balance-data"
        # The name/ID for the new entity
        name = balanceDataEntity

        # Cloud Datastore key
        task_key = datastore_client.key(kind, name)

        # Saves the entity
        balanceJson = datastore_client.get(task_key)

        balanceValue = balanceJson['lastBalanceValue']

    except Exception as err:
        print(err)
        send_notification("get_last_balance%20Error-please%20check%20logs")
        sys.exit(1)

    return balanceValue



def send_notification(message):
    """Sends notification in Telegram group"""

    try:
        req = requests.post(f"https://api.telegram.org/bot{telegramBotToken}/sendMessage?chat_id={telegramGroupId}&text={message}")

    except HTTPError as http_err:
        print(f'HTTP error occurred: {http_err}')
        return http_err

    except Exception as err:
        print(f'Other error occurred: {err}')
        return err

    return req



def get_wallet_balance(adr):
    """Uses xchscan blockchain API to get current chia wallet balance"""

    chiaWalletApiEndpoint = "https://xchscan.com/api/account/balance?"
    chiaApiParams = {'address':adr}

    try:
        response = requests.get(chiaWalletApiEndpoint, params=chiaApiParams)
        responseJson = response.json()
        walletB = responseJson["xch"]

    except HTTPError as http_err:
        print(f'HTTP error occurred: {http_err}')
        send_notification("get_wallet_balance%20Error-please%20check%20logs")
        sys.exit(1)
            
    except Exception as err:
        print(f'Other error occurred: {err}')
        send_notification("get_wallet_balance%20Error-please%20check%20logs")
        sys.exit(1)

    return walletB



def check_wallet(event, context):
    """Triggered from a message on a Cloud Pub/Sub topic.
    Args:
         event (dict): Event payload.
         context (google.cloud.functions.Context): Metadata for the event.
    Checks Chia Wallet for recent changes
    """
    #subsub_message = base64.b64decode(event['data']).decode('utf-8')
    #print(pubsub_message)

    # Load .env File
    try:
        load_dotenv()
    except Exception as e:
        print(e)
    
    # Load Env Vars
    try:
        global telegramBotToken
        global telegramGroupId
        global balanceDataEntity
        framingAdr = os.environ.get("CHIA_WALLET_ID")
        telegramBotToken = os.environ.get("TELEGRAM_API_KEY")
        telegramGroupId = os.environ.get("TELEGRAM_GROUP_ID")
        balanceDataEntity = os.environ.get("BALANCE_DATA_ENTITY")
        print(f"ENV framingAdr: {framingAdr}")
        print(f"ENV telegramBotToken: {telegramBotToken}")
        print(f"ENV telegramGroupId: {telegramGroupId}")
        print(f"ENV balanceDataEntity: {balanceDataEntity}")

        if ( (framingAdr is None) or (telegramBotToken is None) or (telegramGroupId is None) or (balanceDataEntity is None) ):
            raise Exception("env var is none")

    except Exception as err:
        print(err)
        send_notification("Env%20Var%20Error-please%20check%20logs")
        sys.exit(1)
    

    # Get current balance
    currentBalance = get_wallet_balance(framingAdr)
    print(f"Current Balance: {currentBalance}")

    # Get Last Balance
    lastBalance = get_last_balance()
    print(f"Last Balance: {lastBalance}")

    # Compare balance
    if currentBalance != lastBalance:

        # Update Balance Datastore
        update_balance(currentBalance)
        
        if currentBalance > lastBalance:
            
            # Log
            print("New Balance is higher!")
            print("Send Telegram Message..")

            # Send Notification
            winMessage = f"Neuer%20WIN!%20👏🎉🍾%0AWallet%20Balance:%20{currentBalance}%0A%0Ahttps://www.chiaexplorer.com/blockchain/address/{framingAdr}"
            send_notification(winMessage)
        